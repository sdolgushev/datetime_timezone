<?php
/**
 * @package DatetimeTimezone
 * @class   TimeTimezoneType
 * @author  Serhey Dolgushev <dolgushev.serhey@gmail.com>
 * @date    08 Oct 2013
 **/

class TimeTimezoneType extends eZTimeType
{
	const DATA_TYPE_STRING = 'time_timezone';
	const TIMEZONE_FIELD   = 'data_text';
	const TIMEOFDAY_FIELD  = 'data_int';

	public function __construct() {
		$this->eZDataType(
			self::DATA_TYPE_STRING,
			ezpI18n::tr( 'extension/datetime_timezone', 'Time with timezone' ),
			array( 'serialize_supported' => true )
		);
	}

	public function fetchObjectAttributeHTTPInput( $http, $base, $contentObjectAttribute ) {
		$r = parent::fetchObjectAttributeHTTPInput( $http, $base, $contentObjectAttribute );
		if( $r === false ) {
			return false;
		}

		$defaultTimezone = date_default_timezone_get();

		$field    = $base . '_time_timezone_' . $contentObjectAttribute->attribute( 'id' );
		$timezone = $http->postVariable( $field, $defaultTimezone );
		$contentObjectAttribute->setAttribute( self::TIMEZONE_FIELD, $timezone );

		return true;
	}

    public function isInformationCollector() {
		return false;
    }
}

eZDataType::register( TimeTimezoneType::DATA_TYPE_STRING, 'TimeTimezoneType' );
