<?php
/**
 * @package DatetimeTimezone
 * @class   DatetimeTimezoneType
 * @author  Serhey Dolgushev <dolgushev.serhey@gmail.com>
 * @date    07 Oct 2013
 **/

class DatetimeTimezoneType extends eZDateTimeType
{
	const DATA_TYPE_STRING = 'datetime_timezone';
	const TIMEZONE_FIELD   = 'data_text';
	const TIMESTAMP_FIELD  = 'data_int';

	public function __construct() {
		$this->eZDataType(
			self::DATA_TYPE_STRING,
			ezpI18n::tr( 'extension/datetime_timezone', 'Date and time with timezone' ),
			array( 'serialize_supported' => true )
		);
	}

	public function fetchObjectAttributeHTTPInput( $http, $base, $contentObjectAttribute ) {
		$r = parent::fetchObjectAttributeHTTPInput( $http, $base, $contentObjectAttribute );
		if( $r === false ) {
			return false;
		}

		$defaultTimezone = date_default_timezone_get();

		$field    = $base . '_datetime_timezone_' . $contentObjectAttribute->attribute( 'id' );
		$timezone = $http->postVariable( $field, $defaultTimezone );
		$contentObjectAttribute->setAttribute( self::TIMEZONE_FIELD, $timezone );

		$timestamp = (int) $contentObjectAttribute->attribute( self::TIMESTAMP_FIELD );
		if( $timestamp > 0 ) {
			$convertedTimestamp = self::converTimezone( $timezone, $defaultTimezone, $timestamp );
			$contentObjectAttribute->setAttribute( self::TIMESTAMP_FIELD, $convertedTimestamp );
		}

		return true;
	}

	public function objectAttributeContent( $contentObjectAttribute ) {
		$defaultTimezone = date_default_timezone_get();
		$timestamp = $contentObjectAttribute->attribute( self::TIMESTAMP_FIELD );
		$timezone  = $contentObjectAttribute->attribute( self::TIMEZONE_FIELD );

		// Check if any timezone was selected
		try{
			new DateTimeZone( $timezone );
		}catch( Exception $e ){
			$timezone = $defaultTimezone;
		}

		$convertedTimestamp = self::converTimezone( $defaultTimezone, $timezone, $timestamp );
		$dateTime = new eZDateTime();
		$dateTime->setTimeStamp( $convertedTimestamp );
		return $dateTime;
	}

    public function isInformationCollector() {
		return false;
    }

	private static function converTimezone( $inTimezone, $outTimezone, $timestamp ) {
		$iDTZ = new DateTimeZone( $inTimezone );
		$oDTZ = new DateTimeZone( $outTimezone );

		$d = new DateTime( 'now', $iDTZ );
		$d->setTimestamp( $timestamp );

		// $d->getTimestamp is always in UTC
		// http://stackoverflow.com/questions/8668108/how-to-get-unix-timestamp-in-php-based-on-timezone
		$oldOffset = $d->getOffset();
		$d->setTimezone( $oDTZ );
		$newOffset = $d->getOffset();

		return $d->getTimestamp() - $oldOffset + $newOffset;
	}
}

eZDataType::register( DatetimeTimezoneType::DATA_TYPE_STRING, 'DatetimeTimezoneType' );
