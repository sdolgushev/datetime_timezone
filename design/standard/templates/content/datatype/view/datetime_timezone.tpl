{if $attribute.content.is_valid}
{if $attribute.contentclass_attribute.data_int2|eq(1)}
{$attribute.content.timestamp|l10n(shortdate)} {$attribute.content.timestamp|l10n(time)}, {$attribute.data_text}
{else}
{$attribute.content.timestamp|l10n(shortdatetime)}, {$attribute.data_text}
{/if}
{/if}