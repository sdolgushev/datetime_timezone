<?php
/**
 * @package DatetimeTimezone
 * @class   datetime_timezoneInfo
 * @author  Serhey Dolgushev <dolgushev.serhey@gmail.com>
 * @date    07 Oct 2013
 **/

class datetime_timezoneInfo
{
	public static function info() {
		return array(
			'Name'      => 'Datetime datatype with timezone support',
			'Version'   => '1.0',
			'Author'    => 'Serhey Dolgushev',
			'Copyright' => 'Copyright &copy; ' . date( 'Y' ) . ' <a href="http://ua.linkedin.com/in/serheydolgushev" target="blank">Serhey Dolgushev</a>'
		);
	}
}
